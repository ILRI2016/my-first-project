	var app = angular.module("myApp", []);
	
	app.controller("myCtrl", function($scope){
	
		$scope.tasks = [];
		$scope.all = [];
		$scope.completed = [];
				
		$scope.addTask = function(){
			if (!$scope.task) {return;}
			$scope.tasks.push({
				"name":$scope.task,
				"isDone": false
			});
			$scope.all.push({
				"name":$scope.task,
				"isDone": false
			});
			$scope.completed.push({
				"name":$scope.task,
				"isDone": true
			});
		$scope.task = "";
		};
	
		$scope.showall = function (){
			$scope.tasks = $scope.all;
			
		};
		$scope.clearcomp = function (){
			$scope.tasks = $scope.tasks.filter(function(item){
				return !item.isDone;
			});		
		}
		$scope.complete = function (){
			$scope.tasks = $scope.all.filter(function(item){
				return item.isDone;
			});		
			
		}
		$scope.active = function (){
			$scope.tasks = $scope.all.filter(function(item){
				return !item.isDone;
			});			
		};
		$scope.contentedit = function(){
			event.target.contentEditable = event.target.contentEditable == "false" ? "true" : "false";	
		};
		$scope.removeItem = function (x) {
		$scope.tasks.splice(x, 1);	
	};
});