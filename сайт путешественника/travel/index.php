		<?php get_header();?>
            <div class="content">
								<?php if (have_posts()): ?>
									<?php while(have_posts()) : the_post(); ?>
				<div class="post-main">
					<h1><a href="<?php the_permalink (); ?>"> <?php the_title ();?></a> <span><?php the_date_xml(); ?></span> </h1>
					<div class="post">
					    <img src="<?php bloginfo ('template_url');  ?>/images/content.jpg" class="imgstyle" alt="Букенгемский дворец" /> 
						<?php the_excerpt (); ?>
						<p><a href="<?php the_permalink();  ?>">Читать далее</a></p>
						<p> Метки:<a href="#">  Англия</a>,<a href="#">Замки</a>,<a href="#">Дворец</a></p>
					</div>
										
				</div>
						<?php endwhile; ?>
							<div class="nav">
								<?php posts_nav_link(); ?>
									</div>
								<?php endif ; ?>
					</div>
				<?php get_sidebar ();?>
			</div>	
			<? get_footer ();?>
